<?php

namespace Nathanknz\DataNibbler;

use Nathanknz\DataNibbler\Exception\NibblerException;

class Nibbler
{
    protected $buffer = [];
    protected $state = 0;
    protected $message = '';
    protected $callback;
    protected $boundaryBytes = [];
    protected $trimBoundaryBytes = [];

    public function __construct()
    {
        $this->boundaryBytes = [
            'start' => [],
            'end' => [],
        ];

        $this->trimBoundaryBytes = [
            'start' => false,
            'end' => false,
        ];
    }

    public function feed($input)
    {
        if (!count($this->getEndBytes())) {
            throw new NibblerException('End byte not registered');
            return;
        }

        if (!$this->callback) {
            throw new NibblerException('Callback not registered');
            return;
        }

        for ($i = 0; $i < strlen($input); $i++) {
            $this->buffer[] = substr($input, $i, 1);
        }

        while (count($this->buffer)) {
            // reset
            if ($this->state == 0) {
                $this->message = '';
                $this->state = 1;
            }

            // skip start byte if not required
            if ($this->state == 1) {
                if (!count($this->getStartBytes())) {
                    $this->state = 2;
                }
            }

            $byte = array_shift($this->buffer);

            switch ($this->state) {
                case 1: // start byte
                    if ($this->isStartByte($byte)) {
                        if (!$this->isTrimStartByte()) {
                            $this->message .= $byte;
                        }
                        $this->state = 2;
                    }
                    break;
                case 2: // message body
                    if (!$this->isEndByte($byte) || !$this->isTrimEndByte()) {
                        $this->message .= $byte;
                    }
                    if ($this->isEndByte($byte)) {
                        call_user_func_array($this->callback, [$this->message]);
                        $this->state = 0;
                    }
                    break;
            }
        }
    }

    public function registerCallback(callable $callback)
    {
        $this->callback = $callback;

        return $this;
    }

    public function registerBoundaryByte($boundary, $byte)
    {
        if (!isset($this->boundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } elseif (strlen($byte) != 1) {
            throw new NibblerException('Invalid boundary byte length');
        } else {
            if (!in_array($byte, $this->boundaryBytes[$boundary])) {
                $this->boundaryBytes[$boundary][] = $byte;
            }
        }

        return $this;
    }

    public function unregisterBoundaryByte($boundary, $byte)
    {
        if (!isset($this->boundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } elseif (strlen($byte) != 1) {
            throw new NibblerException('Invalid boundary byte length');
        } else {
            foreach (array_keys($this->boundaryBytes[$boundary]) as $key) {
                if ($this->boundaryBytes[$boundary][$key] == $byte) {
                    unset($this->boundaryBytes[$boundary][$key]);
                }
            }
        }

        return $this;
    }

    public function registerStartByte($byte)
    {
        return $this->registerBoundaryByte('start', $byte);
    }

    public function unregisterStartByte($byte)
    {
        return $this->unregisterBoundaryByte('start', $byte);
    }

    public function registerEndByte($byte)
    {
        return $this->registerBoundaryByte('end', $byte);
    }

    public function unregisterEndByte($byte)
    {
        return $this->unregisterBoundaryByte('end', $byte);
    }

    public function getBoundaryBytes($boundary)
    {
        if (!isset($this->boundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } else {
            return $this->boundaryBytes[$boundary];
        }
    }

    public function getStartBytes()
    {
        return $this->getBoundaryBytes('start');
    }

    public function getEndBytes()
    {
        return $this->getBoundaryBytes('end');
    }

    public function isBoundaryByte($boundary, $byte)
    {
        if (!isset($this->boundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } elseif (strlen($byte) != 1) {
            throw new NibblerException('Invalid boundary byte length');
        } else {
            return in_array($byte, $this->boundaryBytes[$boundary]);
        }
    }

    public function isStartByte($byte)
    {
        return $this->isBoundaryByte('start', $byte);
    }

    public function isEndByte($byte)
    {
        return $this->isBoundaryByte('end', $byte);
    }

    public function trimBoundaryByte($boundary, bool $trim)
    {
        if (!isset($this->trimBoundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } else {
            $this->trimBoundaryBytes[$boundary] = $trim ? true : false;
        }

        return $this;
    }

    public function trimStartByte(bool $trim)
    {
        return $this->trimBoundaryByte('start', $trim);
    }

    public function trimEndByte(bool $trim)
    {
        return $this->trimBoundaryByte('end', $trim);
    }

    public function isTrimBoundaryByte($boundary)
    {
        if (!isset($this->trimBoundaryBytes[$boundary])) {
            throw new NibblerException('Invalid boundary');
        } else {
            return $this->trimBoundaryBytes[$boundary];
        }
    }

    public function isTrimStartByte()
    {
        return $this->isTrimBoundaryByte('start');
    }

    public function isTrimEndByte()
    {
        return $this->isTrimBoundaryByte('end');
    }
}
