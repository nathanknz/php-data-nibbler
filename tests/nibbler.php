<?php

include_once(__DIR__.'/../vendor/autoload.php');

$nibbler = new Nathanknz\DataNibbler\Nibbler();

$nibbler->registerStartByte("?")->registerStartByte("!");
$nibbler->registerEndByte(".")->registerEndByte("\r")->registerEndByte("\n");

$nibbler->trimStartByte(true);
$nibbler->trimEndByte(true);

$nibbler->registerCallback(function ($message) {
    echo "Message received by callback:\n";
    echo $message."\n";
});

$chunks = [
    "this is some stuff before the start byte",
    "?",
    "FirstMessageBody\n",
    "some stuff between",
    " messages!",
    "SecondMessageBody",
    ".",
    "\r",
];

foreach ($chunks as $chunk) {
    $nibbler->feed($chunk);
}
